# Starting Reference: http://nlp.seas.harvard.edu/2018/04/03/attention.html#greedy-decoding
import torch.nn as nn
import torch.optim as optim
from datasets import *
from transformer import Transformer

if __name__ == "__main__":
    # 生成数据集
    enc_inputs, dec_inputs, dec_outputs = make_data()
    # 将数据集封装成dataloader
    loader = Data.DataLoader(MyDataSet(enc_inputs, dec_inputs, dec_outputs), 2, True)
    # 创建Transformer模型实例并放到GPU上
    model = Transformer().cuda()
    # 定义损失函数为交叉熵，忽略填充位的计算
    criterion = nn.CrossEntropyLoss(ignore_index=0)
    # 定义优化器为随机梯度下降
    optimizer = optim.SGD(model.parameters(), lr=1e-3, momentum=0.99)

    # 开始训练循环
    for epoch in range(50):
        # 遍历数据集，每个batch为一个训练单位
        for enc_inputs, dec_inputs, dec_outputs in loader:  # enc_inputs : [batch_size, src_len]
                                                            # dec_inputs : [batch_size, tgt_len]
                                                            # dec_outputs: [batch_size, tgt_len]
            # 将数据移动到GPU上
            enc_inputs, dec_inputs, dec_outputs = enc_inputs.cuda(), dec_inputs.cuda(), dec_outputs.cuda()
            # 使用模型进行前向传播，得到预测结果和注意力权重矩阵
            outputs, enc_self_attns, dec_self_attns, dec_enc_attns = model(enc_inputs, dec_inputs)
            # 计算损失
            # outputs  [batch_size * tgt_len, tgt_vocab_size]
            # outputs, dec_outputs.view(-1): 将dec_outputs张量展平成一个一维张量，方便计算交叉熵损失
            loss = criterion(outputs, dec_outputs.view(-1))
            # 打印损失信息
            print('Epoch:', '%04d' % (epoch + 1), 'loss =', '{:.6f}'.format(loss))
            # 梯度清零
            optimizer.zero_grad()
            # 反向传播计算梯度
            loss.backward()
            # 更新模型参数
            optimizer.step()
    # 保存模型
    torch.save(model, 'model.pth')
    print("保存模型")

